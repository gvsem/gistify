
![logo](logo256.png)
# Gistify 1.0

We greet you, strangers outside ITMO University!
Today we present an amazing and powerful tool to manage your snippets at Pastebin and Github Gists in your Visual Studio Code.

Brought to you with :heart: from [MrGeorgeous](https://gitlab.com/MrGeorgeous) & [jvstme](https://gitlab.com/jvstme)

## Requirements

To use Gistify with Pastebin, you must specify several tokens manually:

* `gistify.pastebin.apiToken`: API token must be received at [Pastebin Docs](https://pastebin.com/doc_api#1)


## Features

Gistify can upload your snippets. Use our menu:

![Menu usage](images/menu.png)

You can do selections and publish them:

![RMB usage](images/editor_context_menu.jpg)

Track your snippets in a convinient way:

![RMB usage](images/tracker.jpg)

> Notice: you can not track several types of files. ![RMB usage](images/tracker_warning.jpg)


-----------------------------------------------------------------------------------------------------------

## Known Issues

Contact us at [our issue email](mailto:incoming+mrgeorgeous-gistify-27337429-issue-@incoming.gitlab.com) if you have encountered any problems.

## Release Notes

We are not magicians, we are still students.

### 1.0.0

Initial release of Gistify.
* Upload snippets at Pastebin and Gists (Ctrl+Alt+U)
* Upload selections using RBM in editor
* Track uploaded snippets in View


